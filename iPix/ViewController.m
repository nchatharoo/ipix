//
//  ViewController.m
//  iPix
//
//  Created by Nadheer Chatharoo on 08/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>

@interface ViewController ()

@property (strong, nonatomic) NSString* userID;
@property (strong, nonatomic) NSString *userPass;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([PFUser currentUser]) {
        [self performSegueWithIdentifier:@"pushWelcomeScreen" sender:self];
    }
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    [PFUser logInWithUsernameInBackground:self.username.text password:self.password.text block:^
     (PFUser *user, NSError *error) {
            if (user) {
                [self performSegueWithIdentifier:@"pushWelcomeScreen" sender:self];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            }];
}

- (IBAction)signup:(id)sender {
}

- (IBAction)facebooklogin:(id)sender {
    NSArray *permissionArray = @[@"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];
    // login PFUser using facebook
    [PFFacebookUtils logInWithPermissions:permissionArray block:^(PFUser *user, NSError *error) {
        if (!user) {
            if (!error) {
            NSLog(@"the user cancelled Facebook login");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log in error" message:@"the user cancelled Facebook login" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
            [alert show];
        }
        else
        {
            NSLog(@"error occured %@", error.localizedDescription);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log in error" message:@"cancelled Facebook login, please autorize iPix to access your profile" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
            [alert show];
        }
    }
        else if (user.isNew) {
            NSLog(@"User with Facebook signed up and logged in");
            [self updateUser];
            [self performSegueWithIdentifier:@"pushWelcomeScreen" sender:self];
        }
        else
        {
            NSLog(@"User with facebook login");
            [self performSegueWithIdentifier:@"pushWelcomeScreen" sender:self];
        }
    }];
}

-(void)updateUser {
    FBRequest *request = [FBRequest requestForMe];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            NSDictionary *userData = (NSDictionary *)result;
            self.userID = userData[@"name"];
            [PFUser currentUser][@"displayName"] = self.userID;
            [PFUser currentUser][@"email"] = userData[@"email"];
            [PFUser currentUser].username = self.userID;
            [[PFUser currentUser] saveEventually];
        }
    }];
}
@end
