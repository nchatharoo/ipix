//
//  NCWallImageViewController.m
//  iPix
//
//  Created by Nadheer Chatharoo on 21/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import "NCWallImageViewController.h"
#import "NCImageCell.h"
#import <Parse/Parse.h>
@interface NCWallImageViewController ()

@property (strong, nonatomic) NSMutableArray *imagesArray;

@end

@implementation NCWallImageViewController

- (void)viewWillAppear:(BOOL)animated {
    self.title = @"Wall images";
    [self query];
}

-(void)query {
    PFQuery *query = [PFQuery queryWithClassName:@"images"];
    self.imagesArray = [NSMutableArray arrayWithCapacity:1000];
    self.imagesArray = [query.findObjects mutableCopy];
    [self.mainCollectionView reloadData];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.imagesArray.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NCImageCell *cell = (NCImageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"NCImageCell" forIndexPath:indexPath];
    PFObject *imageObject = [self.imagesArray objectAtIndex:indexPath.row];
    PFFile *imageFile = [imageObject objectForKey:@"image"];
    [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            cell.image.image = [UIImage imageWithData:data];
            
        }
    }];
    NSDictionary *imageDictionary = [self.imagesArray objectAtIndex:indexPath.row];
    cell.nameLabel.text = [imageDictionary objectForKey:@"name"];
    cell.locationLabel.text = [imageDictionary objectForKey:@"location"];
    cell.commentLabel.text = [imageDictionary objectForKey:@"comment"];
    return cell;
}
@end
