//
//  NCShareViewController.m
//  iPix
//
//  Created by Nadheer Chatharoo on 10/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import "NCShareViewController.h"
#import <Parse/Parse.h>

@interface NCShareViewController () <UIAlertViewDelegate>

@end

@implementation NCShareViewController


//Dismiss the keyboard
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Share";
    [self.spinner setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//Save the image using GCD and parse framework
- (IBAction)done:(id)sender {
    NSData *pictureData = UIImageJPEGRepresentation(self.image, 0.5);
    PFFile *image = [PFFile fileWithName:@"img" data:pictureData];
    
    [self.spinner setHidden:NO];
    [self.spinner startAnimating];
    
    dispatch_queue_t queryQueue = dispatch_queue_create("query", NULL);
    dispatch_async(queryQueue, ^{
        PFObject *imageObject = [PFObject objectWithClassName:@"images"];
        imageObject[@"image"] = image;
        imageObject[@"name"] = self.photoName.text;
        imageObject[@"location"] = self.photoLocation.text;
        imageObject[@"comment"] = self.photoComment.text;

        
        [image saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [imageObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    [self.spinner stopAnimating];
                    [self.spinner setHidden:YES];
                    [[[UIAlertView alloc] initWithTitle:nil message:@"Image uploaded" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                } else {
                    [[[UIAlertView alloc] initWithTitle:nil message:[error userInfo][@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil] show];
                }
            }];
        }progressBlock:^(int percentDone) {
            NSLog(@"%d", percentDone);
        }];
    });
    [self.view endEditing:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self performSegueWithIdentifier:@"wallView" sender:self];
    }
}
@end
