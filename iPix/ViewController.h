//
//  ViewController.h
//  iPix
//
//  Created by Nadheer Chatharoo on 08/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
- (IBAction)login:(id)sender;
- (IBAction)facebooklogin:(id)sender;

@end
