//
//  NCWallImageViewController.h
//  iPix
//
//  Created by Nadheer Chatharoo on 21/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCWallImageViewController : UIViewController  <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *mainCollectionView;

@end
