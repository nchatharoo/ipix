//
//  main.m
//  iPix
//
//  Created by Nadheer Chatharoo on 08/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
