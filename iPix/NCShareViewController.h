//
//  NCShareViewController.h
//  iPix
//
//  Created by Nadheer Chatharoo on 10/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCShareViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) UIImage *image;

@property (strong, nonatomic) IBOutlet UITextField *photoName;
@property (strong, nonatomic) IBOutlet UITextField *photoLocation;
@property (strong, nonatomic) IBOutlet UITextView *photoComment;
- (IBAction)done:(id)sender;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@end
