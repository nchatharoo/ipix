//
//  NCSignupViewController.h
//  iPix
//
//  Created by Nadheer Chatharoo on 09/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCSignupViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *email;
- (IBAction)signup:(UIButton *)sender;

@end
