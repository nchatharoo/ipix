//
//  NCWelcomeViewController.h
//  iPix
//
//  Created by Nadheer Chatharoo on 09/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCWelcomeViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
- (IBAction)album:(id)sender;
- (IBAction)camera:(id)sender;

@end
